# dynacv

This is a free adaption from the work of https://github.com/ddbullfrog/resumecard.

I kept the bootstrap's theme and I used Vue.js framework instead of Jekyll.

# Fonctionnalities

- It's a Javascript CV based on Vue.js framework,
- It displays a dynamic cv according to a professional profil selected inside the dropbox available at the top page of cv.html file,
- It displays a friendly view for printing,
- You can sort your abilities according to Dreyfus model.

# How to use it

First you need to define your professionnal profiles like I did inside db-cv.js.
Then you need to adapt your template inside cv.html.


I don't make it *very easy* but with a little affort I think it is possible to someone else to create its own cv based on it.

# IMPORTANT
Dans le cas d'un hébergement chez le FAI Free, seul php5 est activé.
Il faudra pour faire fonctionner le projet commenté la ligne 136 du fichier vendor/symfony/polyfill-mbstring/bootstrap.php.

//function mb_str_pad(string $string, int $length, string $pad_string = null, int $pad_type = STR_PAD_RIGHT, ?string $encoding = null): string { return p\Mbstring::mb_str_pad($string, $length, $pad_string, $pad_type, $encoding); }


