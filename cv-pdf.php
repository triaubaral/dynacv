<?php

require __DIR__.'/vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;
use Symfony\Component\DomCrawler\Crawler;

session_start();

if(isset($_POST["HTMLcontent"])){

	$content = $_POST["HTMLcontent"];

	$_SESSION["HTMLcontent"] = $content;

}
else{

	$content = $_SESSION["HTMLcontent"];

}

$crawler = new Crawler($content);

$crawler->filter('html script')->each(function (Crawler $crawler) {
    foreach ($crawler as $node) {
        $node->parentNode->removeChild($node);
    }
});

$crawler->filter('html form')->each(function (Crawler $crawler) {
    foreach ($crawler as $node) {
        $node->parentNode->removeChild($node);
    }
});

$crawler->filter('html head')->each(function (Crawler $crawler) {
    foreach ($crawler as $node) {
        $node->parentNode->removeChild($node);
    }
});

$filteredContent = $crawler->html();

$footer= "<page_footer>
				<span style=\"text-align:right\">Aurélien Tricoire - CV : [[page_cu]]/[[page_nb]]</span>
		</page_footer>
  ";
$newContent = str_replace("<body>","<page style=\"font-size: 12px\">",$filteredContent);
$newContent = str_replace("</body>","</page>", $newContent);
$cssPrint = file_get_contents("assets/css/print.css");
$styleSection = "<style type=\"text/css\">\n<!--\n$cssPrint\n-->\n</style>\n";



try {
	
$html2pdf = new Html2Pdf('P', 'A4', 'fr', true, 'UTF-8');
$html2pdf->pdf->SetDisplayMode('fullpage');
$html2pdf->setDefaultFont('Arial');
$html2pdf->writeHTML($styleSection.$newContent);
$html2pdf->output("Aurélien Tricoire-CV.pdf");

} catch (Html2PdfException $e) {
    $html2pdf->clean();
    $formatter = new ExceptionFormatter($e);
    echo $formatter->getHtmlMessage();
}
