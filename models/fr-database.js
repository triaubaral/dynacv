function createSectionData(profil){
	
	var dataProfil = new Map();
	var currentProfil = findProfilGroupByName(profil);
	
	/** !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * Tout ajout dans la map dataProfil
	 * doit avoir un record dans section_table !!
	 *
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/ 
	
	dataProfil.set("Expérience",currentProfil.postes);
	dataProfil.set("Compétence",currentProfil.competences);
	dataProfil.set("Formation",formation_table);
	dataProfil.set("Culture",culture_table);
	dataProfil.set("Divers",divers_table);	
	dataProfil.set("Langues",langue_table);
	dataProfil.set("Légende", legend_competence_table);
	//dataProfil.set("Géolocalisation",[]);
		
	var tab = Array.from(section_table);
	
	tab.forEach(function(line) {
		
		/*if(line.name == "Géolocalisation"){
					   
		   line['address'] = "6, allée de l'Herbaudière 31770 Colomiers";
		   line['place']="https://www.google.com/maps/embed/v1/place?key=AIzaSyClEWLh20lxrQBFR7omuuVESviszPxONyk&q="+line['address'];	
			
		}*/
	
			
		line['data'] = dataProfil.get(line.name);
	
	});
	
	return tab;

}

function findDataCollection(table, keys){
	
	var itemsFound = [];
	
	keys.forEach(function(key) {
		
		var itemFound = findData(table, key);
		
		if(itemFound)
			itemsFound.push(itemFound);
	
	});
	
	return itemsFound;

}

function findData(table, key){
	
	var itemFound = false;
		
	table.forEach(function(item) {
		if(item.name === key){
			itemFound = item;	
		}
	});
	
	
	return itemFound;

}

function findPosteByName(name){
	return findData(poste_table, name);
}

function findMissionsByNames(names){	
	return findDataCollection(mission_table, names);
}

function findCompetencesByNames(names){	
	return findDataCollection(competence_table, names);	
}

function findCompetenceByName(name){
	return findData(competence_table, name);
}

function findProfilGroupByName(name){
	return findData(profil_group_table, name);
}

function findSectionByName(name){
	return findData(section_table, name);
}

function findProfilsByNames(names){
	
	return findDataCollection(profil_name_table, names);
}

function findProfilNameById(id){
	
	profil_name_table.forEach(function(profil){
		
		if(profil.id === id)
			return profil.name;
		
	});
	
	return findProfilNameById(1);
	
}

function findIdProfilByName(name){
	
	for(var i = 0; i<profil_name_table.length; i++){
		if(profil_name_table[i] === name)
			return i;
		
	}
	
	return 0;
	
}

function sortCompetenceByGroupAndLevel(groupCompetences, level){

  var data = [];	
  
  groupCompetences.forEach(function(group){	
	    
	  data[group.name]= sortCompetenceByLevel(group.competences,level);
  });
  
  return data;
	
}

function sortCompetenceByLevel(competences, level){

   	
   	var itemsFound = [];
	
	competences.forEach(function(key) {
		
		if(key.level == level)
			itemsFound.push(key.name);
	
	});
	
	return itemsFound;
	
}

var competence_level_table = [

    {
		name : "Expert",
		rate : 100
	},
	
	{
		name : "Compétent",
		rate : 80
	},
	
	{
		name : "Avancé",
		rate : 60
	},
	
	{
		name : "Basique",
		rate : 40
	},
	
	{
		name : "Débutant",
		rate : 20
	}
		
];


var cartouche_data = {							 
	 nom:"Aurélien Tricoire",
	 mail:"aurelien.tricoire@gmail.com",
	 tel:"06-77-78-85-19",
	 siteperso:"http://aurelien.tricoire.free.fr",
	 adr:"6, allée de l'Herbaudière",
	 cp:"31770",
	 ville:"Colomiers / Haute-Garonne, France",
	 age:"43",
	 nbEnfant:"1 enfant",
	 situationperso:"marié",
	 situationpro:"en poste chez Mipih"
};

var profil_name_table = [

	{id:1, type:"moe", name:"Ingénieur Logiciel"}, 
	{id:2, type:"moe", name:"Expert Technique"}, 
	{id:3, type:"moe", name:"Développeur Sénior"}, 
	{id:4, type:"moe", name:"Architecte Applicatif"}, 
	{id:5, type:"moe", name:"Responsable Technique"}, 
	{id:6, type:"moe", name:"Intégrateur d'application"},
	{id:7, type:"moa", name:"Product Owner"}, 
	{id:8, type:"moa", name:"Chef de projet"}, 
	{id:9, type:"moa", name:"Formateur"},
	{id:10, type:"pilote", name:"Responsable de pôle"}

];

var legend_competence_table = [
{
   name :'Débutant',              
   description :[
     		 `Il sait utiliser la technologie afin de réaliser des scénarios en suivant des "recettes" ou des "expériences" trouvées sur des ressources externes.`,
     		 `Il sait utiliser la technologie dans un "contexte" spécifique au problème qu'il doit résoudre."`,    
             `Il ne sait pas "improviser" pour réaliser un scénario nominal non répertoriée dans des "recettes" ou "expériences" trouvées sur des ressources externes.`,
             `Il ne sait pas "anticiper" ni "dépanner" un scénario exceptionnel non répertoriée dans les "recettes" ou "expériences" trouvées sur des ressources externes.`,
             `Il ne possède aucune expérience significative avec la technologie.`
    ]
},            

{
	name: 'Basique',
	description :[   
            `Il sait utiliser la technologie afin de réaliser des scénarios en suivant des "recettes" ou des "expériences" trouvées sur des ressources externes.`,
            `Il sait utiliser la technologie dans un "contexte" spécifique au problème qu'il doit résoudre.`,
            `Il sait "improviser" pour réaliser un scénario nominal non répertoriée dans des "recettes" ou "expériences" trouvées sur des ressources externes.`,
            `Il ne sait pas "dépanner" un scénario exceptionnel non répertoriée dans son catalogue de "recettes" ou "expériences".`,
            `Il possède quelques expériences significatives avec la technologie.`,
            `Il sait faire face à un problème rencontré dans un scénario exceptionnel en utilisant sa capacité d'analyse.`
    ]
},
{
	name : 'Avancé',
	description :[    
            `Il sait utiliser la technologie afin de réaliser des scénarios en suivant des "recettes" ou des "expériences" trouvées sur des ressources externes.`,	
			`Il sait "utiliser" les design pattern de la technologie dans différents "contextes".`,
            `Il sait "improviser" pour réaliser un scénario nominal non répertoriée dans des "recettes" ou "expériences" trouvées sur des ressources externes.`,
            `Il sait "dépanner" un scénario exceptionnel en utilisant sa capacité d'analyse.`,
            `Il possède une bonne expérience significative avec la technologie.`,
            `Il sait faire face à un problème rencontré dans un scénario exceptionnel en utilisant sa capacité d'analyse.`
    ]
},
{            
	name : 'Compétent',
    description : [
            `Il sait utiliser la technologie afin de réaliser des scénarios en suivant des "recettes" ou des "expériences" trouvées sur des ressources externe.`,
			`Il sait "créer" et "utiliser" les design pattern de la technologie dans différents "contextes".`,          
            `Il sait "improviser" pour réaliser un scénario nominal non répertoriée dans des "recettes" ou "expériences" trouvées sur des ressources externes.`,
            `Il sait "anticiper" et "dépanner" l'exécution d'un scénario exceptionnel en utilisant sa capacité d'analyse.`,
            `Il possède une expérience importante avec la technologie.`
     ]
},             
{
	name : 'Expert',
	description : [    
            `Il sait utiliser la technologie afin de réaliser des scénarios en suivant des "recettes" ou des "expériences" trouvées sur des ressources externes.`,	
			`Il sait "créer" et "utiliser" les design pattern la technologie dans différents "contextes".`,
            `Il sait "improviser" pour réaliser un scénario nominal non répertoriée dans des "recettes" ou "expériences" trouvées sur des ressources externes.`,
            `Il sait "anticiper" et "dépanner" l'exécution d'un scénario exceptionnel en utilisant son instinct et sa capacité d'analyse.`,
            `Il possède une vaste expérience avec la technologie.`,
            `Il recherche en permanence à améliorer ses "compétences" et "connaissances" sur la technologie.`,
            `Il possède la capacité d'écrire des articles, des livres ou des supports de cours sur la technologie.`,
      ]
}

];

var section_table = [
   {
	name : "Cartouche",
	title : "Curriculum Vitae"	 
   },
   {
	name : "Légende",
	title : "Légende des compétences"	 
   },
   {
	name : "Expérience",
	title : "Expérience professionnelle"
   },
   {
	name : "Formation",
	title : "Formation initiale"
   },
   {
	name : "Compétence",
	title : "Compétences professionnelles"
   },
   {
	name : "Culture",
	title : "Culture informatique"
   },
   {
	name : "Divers",
	title : "Indications complémentaires"
   },
   {
	name : "Langues",
	title : "Langues"
   },
    {
	name : "Géolocalisation",
	title : "Géolocalisation"
   }

];

var poste_table = [
	{
		name:'Product Owner Mipih',
		startYear: '2023',
		firmName:'Mipih',		
		endYear:'Aujourd\'hui',
		jobCityLoc:'Toulouse',
		jobCountryLoc:'France'
	},
	{
		name:'Product Owner iMSA',
		startYear: '2020',
		firmName:'iMSA',		
		endYear:'2023',
		jobCityLoc:'Montauban',
		jobCountryLoc:'France'
	},
	{
		name:'Responsable Technique',
		startYear: '2019',
		firmName:'Capgemini',		
		endYear:'2020',
		jobCityLoc:'Toulouse',
		jobCountryLoc:'France'
	},
	{
		name:'Leader Technique',
		startYear: '2017',
		firmName:'la Cnam',		
		endYear:'2019',
		jobCityLoc:'Angers',
		jobCountryLoc:'France'
	},
	{
		name:'Expert Technique',
		startYear: '2010',
		firmName:'la Cnam',		
		endYear:'2017',
		jobCityLoc:'Angers',
		jobCountryLoc:'France'
	},
	{
		name:'Développeur',
		startYear: '2008',
		firmName:'Cirti',		
		endYear:'2010',
		jobCityLoc:'Nantes',
		jobCountryLoc:'France'
	},
	{
		name:'Technicien Informatique',
		startYear: '2005',
		firmName:'Urssaf',		
		endYear:'2008',
		jobCityLoc:'La Roche sur Yon',
		jobCountryLoc:'France'
	},
		
];

var mission_table = [
	{
	  name:'Responsable Technique',
	  label: 'Responsable Technique',
	  actions:[
		'Elaboration du projet d\'industrialisation des pratiques de développement au sein de la ruche Toulouse',
		'Participation à la mise en oeuvre du projet précédemment élaboré',
		'Coordination technique entre plusieurs équipes de développement',
		'Evangélisation sur des pratiques de développement : eco-conception, mobprogramming et pair programming',
		'Mise en place d\'un système documentaire dynamique basée sur Asciidoc'	   							       
	  ]
	 },						
					
	 {
	  name:'Architecte Applicatif',
	  label: 'Architecte Applicatif',
	  actions:[
	   'Animation des ateliers avec les partenaires pour définir les outils, normes et processus attendus', 
	   'Définition du périmètre de réalisation (Proof Of Concept, Proof Of Value ou Projet) des outils, normes et processus identifiés',
	   'Cadrage technique sur la réalisation des outils, normes et processus identifiés',							       
	   'Animation des équipes Scrum en charge de la réalisation des outils, normes et processus identifiés',							      
	   'Animation des ateliers de suivi de réalisation avec les partenaires' 							       
	  ]
	 },
	 {	     
	  name:'Product Owner',
	  label: 'Product Owner',
	  actions:[
	  'Définition de la vision associée à chaque release produit', 
	  'Définition des users stories', 
	  'Gestion des priorités des user stories'
	  ]
	 },							     
	 {
	  name:'Scrum Master',
	  label: 'Scrum Master',
	  actions:['Préparation et animation des différents cérémoniaux Scrum', 'Formation des équipiers au framework Scrum']
	 },
	 {
	  name:'Equipier-LT',
	  label: 'Equipier',
	  actions:[
	  'Réalisation de prototypes associés aux outils, normes et processus identifiés', 							      
	  'Transfert de connaissance aux autres membres de l\'équipe']
	 },
	  {
	  name:'DevOps',
	  label: 'DevOps',
	  actions:[
	  'Définition du processus de fabrication d\'un binaire', 
	  'Cadrage technique sur l\'utilisation des outils Artifactory, Jenkins et Maven',
	  'Promotion du Test Driven Design et du Domain Driven Design'
	  ]
	 },
	 {
	  name:'Formateur-ET',
	  label: 'Formateur',
	  actions:[
	  'Réalisation des supports de formation sur le processus de fabrication', 							      
	  'Formation auprès des agents sur le processus de fabrication'
	  ]
	 },
	 {
	  name:'Equipier-ET',
	  label: 'Equipier',
	  actions:[
	  'Estimation des charges et réalisation de chaque tâche définie', 
	  'Réalisation d\'une partie des outils associés au processus de fabrication', 
	  'Transfert de connaissance aux autres membres de l\'équipe',
	  'Contribution et support aux outils, normes et processus de la division',	
	  'Assure le support technique niveau 3 des outils associés au processus de fabrication'
	  ]
	 },
	  {	     
	  name:'Développeur',
	  label: 'Développeur',
	  actions:[
	  'Rédaction de spécifications techniques avec UML', 
	  'Implémentation des évolutions et des corrections en Java',
	  'Assure le support technique niveau 2 et 3'
	  ]
	 },
	 {	     
	  name:'Support Technique',
	  label: 'Support Technique',
	  actions:[
	   'Assure le support technique de niveau 1 sur différents produits', 
	   'Réalise des outils en PHP qui sont des réponses opérationnelles à un besoin précis', 
	   'Configuration d\'un outil interne pour gérer les courriers avec les cotisants'
	   ]
	 },	
	 {
	  name:'Administrateur Système',
	  label: 'Administrateur Système',
	  actions:[
	  'Réalisation des scripts Windows pour gérer les ressources partagées', 	
	  'Entretien des différentes imprimantes réseau du parc informatique',						      
	  'Configuration des systèmes de sauvegarde des 26 serveurs',
	  'Réalisation d\'étude pour acheter un onduleur ',
	  'Remise aux normes Cisco CCNA de l\'organisation du branchement des serveurs'
	  ]
	 },						     
	 {
	  name:'Formateur-ST',
	  label: 'Formateur',
	  actions:[
	  'Réalise les supports de cours sur les technologies HTML, CSS, Javascript et PHP', 
	  'Formation des agents du grand ouest de la France sur les technologies HTML, CSS, Javascript et PHP', 
	  ]
	 }
	 	
];

var formation_table = [

	{ 		
			startYear: '2023',
			title:'Anglais',
			certificateTitle:'EF SET Certificate C2 Proficient',
			optionTitle:'https://www.efset.org/cert/M594rh',
			endYear:'2023',
			cityLocation:'En ligne',
			countryLocation:'France',
			description: 'Niveau C2 en anglais.'
			  
	  },
	{ 		
			startYear: '2021',
			title:'Product Owner',
			certificateTitle:'Safe 5 PO/PM Certificate',
			optionTitle:'Product Owner',
			endYear:'2022',
			cityLocation:'Montauban',
			countryLocation:'France',
			description: 'Certification SAFe 5.'
			  
	  },
	  { 		
			startYear: '2021',
			title:'Practitioner SAFe',
			certificateTitle:'SAFe 5 Practitioner Certification',
			optionTitle:'Product Owner',
			endYear:'2022',
			cityLocation:'Montauban',
			countryLocation:'France',
			description: 'Certification SAFe 5.'
			  
	  },

 { 		
			startYear: '2018',
			title:'Scrum Alliance',
			certificateTitle:'Certified Scrum',
			optionTitle:'Product Owner',
			endYear:'2018',
			cityLocation:'Paris',
			countryLocation:'France',
			description: 'Certification obtenue après avoir suivi une formation de 3 jours sur le framework Scrum.'
			  
	  },
							
	  { 		
			startYear: '2012',
			title:'Conservatoire National des Arts et Métiers (le Cnam)',
			certificateTitle:'Diplôme d\'ingénieur informatique',
			optionTitle:'option système d\'informations - mention bien',
			endYear:'2014',
			cityLocation:'Nantes',
			countryLocation:'France',
			description: 'En formation en cours du soir, en dehors de mon temps de travail, '+
			'j\'ai suivi les cours au Cnam afin d\'obtenir le titre d\'ingénieur en informatique dans la spécialité '+
			'systèmes d\'information. Vous pouvez lire mon mémoire sur le thème de la l\'optimisation du processus de '+
			'fabrication d\'applications Java à la Cnam.'
			  
	  },
								  
	   { 		
			startYear: '2002',
			title:'Glyndwûr',
			certificateTitle:'Bachelor of Science Honour degree',
			optionTitle:'Computer Networks',
			endYear:'2004',
			cityLocation:'Wrexham',
			countryLocation:'Pays de Galles',
			description: 'J\'ai obtenu ce diplôme avec mention bien. Ce dipôme obtenu à l\'étranger est '+
			'avant tout un diplôme d\'informatique de gestion général. Le programme était équilibré entre '+
			'des UV liées au développement logiciel et au réseau informatique. En plus, de ce diplôme, j\'ai '+
			'obtenu une certification Pitman qui atteste d\'un niveau équivalent au First Certficate en Anglais de Cambridge.'
			  
	  },
	  
	  { 		
			startYear: '2000',
			title:'Lycée St Pierre la Joliverie',
			certificateTitle:'Brevet de Technicien Supérieur (B.T.S.)',
			optionTitle:'Informatique de gestion option développeur d\'applications',
			endYear:'2002',
			cityLocation:'Nantes',
			countryLocation:'France',
			description: 'Après 4 années en comptabilité, j\'ai souhaité me réorienter vers l\'informatique qui'+
			' m\'attirait par l\'aspect créatif de l\'activité de développeur.'								  
	  },	
	  
	   { 		
			startYear: '1998',
			title:'Lycée St Pierre la Joliverie',
			certificateTitle:'Brevet de Technicien Supérieur (B.T.S.)',
			optionTitle:'Comptabilité-Gestion',
			endYear:'2000',
			cityLocation:'Nantes',
			countryLocation:'France',
			description: ''								  
	  },
	  { 		
			startYear: '1997',
			title:'Lycée Jeanne D\'Arc',
			certificateTitle:'Baccalauréat Sciences et Technologies Tertiaires (S.T.T.)',
			optionTitle:'Comptabilité-Gestion',
			endYear:'1998',
			cityLocation:'Montaigu',
			countryLocation:'France',
			description: ''								  
	  }		

];

var competence_table = [

	 {
		  name:'Asciidoc',
		  level: 'Avancé'
	  },
	  {
		  name:'Klaxoon',
		  level: 'Avancé'
	  },
	   {
		  name:'Jira',
		  level: 'Expert'
	  },
	   {
		  name:'Rally',
		  level: 'Avancé'
	  },
	  {
		  name:'Kahoot',
		  level: 'Débutant'
	  },
	  {
		  name:'Java',
		  level: 'Débutant'
	  },
	  {
		  name:'HTML',
		  level: 'Expert'
	  },
	  {
		  name:'PHP',
		  level: 'Débutant'
	  },
	  {
		  name:'Vue',
		  level: 'Basique'
	  },
	  {
		  name:'CSS',
		  level: 'Basique'
	  },
	  {
		  name:'Groovy',
		  level: 'Basique'
	  },
	  {
		  name:'Javascript',
		  level: 'Basique'
	  },
	  
	  {
		  name:'Scrum',
		  level: 'Expert'
	  },
	  
	  {
		  name:'Spring',
		  level: 'Compétent'
	  },
	  {
		  name:'JUnit',
		  level: 'Compétent'
	  },
	  {
		  name:'Windows',
		  level: 'Compétent'
	  },
	  {
		  name:'Linux',
		  level: 'Compétent'
	  },
	  {
		  name:'Toplink',
		  level: 'Basique'
	  },
	  {
		  name:'Hibernate',
		  level: 'Débutant'
	  },
	  {
		  name:'Doctrine',
		  level: 'Débutant'
	  },
	  {
		  name:'Oracle',
		  level: 'Avancé'
	  },
	  {
		  name:'MySQL',
		  level: 'Basique'
	  },
	  {
		  name:'PostgreSQL',
		  level: 'Débutant'
	  },
	  {
		  name:'Eclipse',
		  level: 'Compétent'
	  },
	    {
		  name:'VSCode',
		  level: 'Compétent'
	  },
	   {
		  name:'ChatGPT',
		  level: 'Débutant'
	  },
	  {
		  name:'Maven',
		  level: 'Compétent'
	  },
	  {
		  name:'Jenkins',
		  level: 'Compétent'
	  },
	  {
		  name:'Artifactory',
		  level: 'Avancé'
	  },
	  {
		  name:'Composer',
		  level: 'Basique'
	  },
	  {
		  name:'Bootstrap',
		  level: 'Débutant'
	  },
	  {
		  name:'UML',
		  level: 'Avancé'
	  },
	  {
		  name:'BDD',
		  level: 'Compétent'
	  }	,
	  {
		  name:'TDD',
		  level: 'Compétent'
	  },
	  {
		  name:'Libre Office',
		  level: 'Compétent'
	  },
	  {
		  name:'Microsoft Office',
		  level: 'Compétent'
	  },
	  {
		  name:'Astah',
		  level: 'Basique'
	  },
	  {
		  name:'SOAP',
		  level: 'Avancé'
	  },
	  {
		  name:'REST',
		  level: 'Avancé'
	  },
	  {
		  name:'Sonar',
		  level: 'Expert'
	  },
	  {
		  name:'AssertJ',
		  level: 'Basique'
	  },
	  {
		  name:'Mockito',
		  level: 'Basique'
	  },
	  {
		  name:'SQL',
		  level: 'Compétent'
	  },
	   {
		  name:'SAFe',
		  level: 'Expert'
	  }
	  

];

var langue_table = [
  {
	 language:'Français',
	 level:'Langue maternelle'
	},
	{
	 language:'Anglais',
	 level:'Courant'
	},
	{
	 language:'Espagnol',
	 level:'Notions'
	}

];

var divers_table = [

	 'Pratique du tennis de table en compétition pendant 25 ans', 
	 'Contributeur occasionnel sur des projets open source',
	 'Adepte du management par la bienveillance'
			 

];

var culture_table = [
	{	     
			  theme:'Livres importants',
			  items:[
			   {text:'Clean code , Robert C. Martin', url:'https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882'},
			   {text:'Code Complete 2, Steve McConnel', url:'https://www.amazon.com/Code-Complete-Practical-Handbook-Construction/dp/0735619670'}, 
			   {text:'UML Distilled: A Brief Guide to the Standard Object Modeling Language, Martin Fowler', url:'https://www.amazon.fr/UML-Distilled-Standard-Modeling-Language-ebook/dp/B000OZ0N8A'},
			   {text:'Growing Object-Oriented Software, Guided by Tests, Steve Freeman', url:'https://www.amazon.com/Growing-Object-Oriented-Software-Guided-Tests/dp/0321503627'},
			   {text:'Living documentation, Cyrille Martraire', url:'https://leanpub.com/livingdocumentation'}
			   ]
	},	
	 {	     
			  theme:'Personnalités influentes',
			  items:[
			   {text:'Andy Hunt', url:'https://toolshed.com/'},
			   {text:'Robert C. Martin', url:'https://blog.cleancoder.com/'},
			   {text:'Martin Fowler', url:'https://martinfowler.com/'},	
			   {text:'Jeff Atwood', url:'https://blog.codinghorror.com/'}
			   ]
	},
	 {	     
			  theme:'Source d\'inspiration',
			  items:[
			   {text:'USI', url:'https://www.youtube.com/user/usievents'}, 
			   {text:'TedEx', url:'https://www.ted.com/topics/computers'}, 
			   {text:'Développez.com', url:'https://www.developpez.com/'},
			   {text:'Mapt.io', url:'https://mapt.packtpub.com/'},
			   {text:'Pragmatic Programmer', url:'https://pragprog.com/'},
			   {text:'Smashing Magazine', url:'https://www.smashingmagazine.com/'},
			   ]
	}

];


var profil_group_table = [
 {  
	id : "1",  
	name : "moe",
    profils :  
		findProfilsByNames([ "Ingénieur Logiciel",
							"Responsable Technique",
							"Expert Technique", 
							"Développeur Sénior", 
							"Architecte Applicatif", 
							"Responsable Technique", 
							"Intégrateur d'application"]),
	postes:[
	{
		 
		 poste : findPosteByName('Responsable Technique'),
		 missions : findMissionsByNames(['Responsable Technique']) 
		  
	  },
	
	  {
		 
		 poste : findPosteByName('Leader Technique'),
		 missions : findMissionsByNames(['Architecte Applicatif', 'Product Owner', 'Scrum Master', 'Equipier-LT']) 
		  
	  },
	  {
		
		 poste : findPosteByName('Expert Technique'),
		 missions : findMissionsByNames(['DevOps', 'Formateur-ET', 'Equipier-ET'])	 
		  
	  },
	  {
		 
		 poste : findPosteByName('Développeur'),
		 missions : findMissionsByNames(['Développeur'])	 
		  
	  },
	  {
		 
		 poste : findPosteByName('Technicien Informatique'),
		 missions : findMissionsByNames(['Support Technique', 'Formateur-ST', 'Administrateur Système'])	 
		  
	  },
	 ],
	 competences :[
		{
		  name:'Langages',
		  competences: findCompetencesByNames(["Java","PHP","HTML","CSS", "UML", "Javascript", "SQL", "Asciidoc"])
		},

		 {
		  name:'Framework',
		  competences: findCompetencesByNames(["SAFe","Scrum","Spring", "Vue", "JUnit", "AssertJ", "BDD", "TDD", "Mockito", "Bootstrap"]) 
		},
		{
		  name:'Système d\'exploitation',
		  competences: findCompetencesByNames(["Windows","Linux"]) 
		  
		},
		{
		  name:'ORM',
		  competences:findCompetencesByNames(["Toplink","Hibernate"])
		  
		},
		{
		  name:'Base de données',
		  competences:findCompetencesByNames(["Oracle","MySQL","PostgreSQL"])
		  
		},

		 {
		  name:'Outils de développement',
		  competences:findCompetencesByNames(["ChatGPT","VSCode", "Eclipse","Maven","Jenkins", "Artifactory", "Sonar", "Composer", "Astah"])
		},
		{
		  name:'WebService',
		  competences:findCompetencesByNames(["SOAP","REST"])
		}
	]
  },
  {   
	id : "2",
	name : "moa",	
	profils : findProfilsByNames(["Product Owner", "Chef de projet", "Formateur"]),
	postes : [ 
	 {
		
		 poste :findPosteByName('Responsable Technique'),
		 missions : findMissionsByNames(['Architecte Applicatif', 'Product Owner', 'Scrum Master']) 
		  
	  },	 
	  {		 
		 poste :findPosteByName('Expert Technique'),
		 missions : findMissionsByNames(['DevOps', 'Formateur-ET'])	 
		  
	  },
	  {
		 poste :findPosteByName('Développeur'),
		 missions : findMissionsByNames(['Développeur'])	 
		  
	  },
	  {
		 poste :findPosteByName('Technicien Informatique'),
		 missions : findMissionsByNames(['Support Technique', 'Formateur-ST'])	 
		  
	  },
	 ],
	 competences :[
		{
		  name:'Langages',
		  competences: findCompetencesByNames(["Java","PHP","HTML","CSS", "Groovy", "Javascript"])
		},

		 {
		  name:'Framework',
		  competences: findCompetencesByNames(["SAFe","Scrum","JUnit", "BDD", "TDD"]) 
		},
		{
		  name:'Système d\'exploitation',
		  competences: findCompetencesByNames(["Windows","Linux"]) 
		  
		},
		{
		  name:'ORM',
		  competences:findCompetencesByNames(["Toplink","Hibernate"])
		  
		},
		{
		  name:'Base de données',
		  competences:findCompetencesByNames(["Oracle","MySQL","PostgreSQL"])
		  
		},
		 {
		  name:'Outils de développement',
		  competences:findCompetencesByNames(["VSCode","Eclipse","Maven","Jenkins", "Artifactory", "Composer"])
		},
		{
		  name:'Bureautique',
		  competences:findCompetencesByNames(["Klaxoon","Kahoot","Jira","Rally","Libre Office","Microsoft Office"])
		}
	]
  },
  {  
	id : "3",  
	name : "pilote",
    profils :  
		findProfilsByNames([ "Ingénieur Logiciel",
							"Responsable Technique"]),
	postes:[
	{
		 
		 poste : findPosteByName('Responsable Technique'),
		 missions : findMissionsByNames(['Responsable Technique']) 
		  
	  },
	
	  {
		 
		 poste : findPosteByName('Responsable Technique'),
		 missions : findMissionsByNames(['Architecte Applicatif', 'Product Owner', 'Scrum Master']) 
		  
	  },
	  {
		
		 poste : findPosteByName('Expert Technique'),
		 missions : findMissionsByNames(['DevOps', 'Equipier-ET'])	 
		  
	  },
	  {
		 
		 poste : findPosteByName('Développeur'),
		 missions : findMissionsByNames(['Développeur'])	 
		  
	  },
	  {
		 
		 poste : findPosteByName('Technicien Informatique'),
		 missions : findMissionsByNames(['Support Technique', 'Formateur-ST', 'Administrateur Système'])	 
		  
	  },
	 ],
	 competences :[
		{
		  name:'Langages',
		  competences: findCompetencesByNames(["Java","PHP","HTML","CSS", "UML", "Javascript", "SQL", "Asciidoc"])
		},

		 {
		  name:'Framework',
		  competences: findCompetencesByNames(["SAFe","Scrum","Spring", "Vue", "JUnit", "AssertJ", "BDD", "TDD", "Mockito", "Bootstrap"]) 
		},
		{
		  name:'Système d\'exploitation',
		  competences: findCompetencesByNames(["Windows","Linux"]) 
		  
		},
		{
		  name:'ORM',
		  competences:findCompetencesByNames(["Toplink","Hibernate"])
		  
		},
		{
		  name:'Base de données',
		  competences:findCompetencesByNames(["Oracle","MySQL","PostgreSQL"])
		  
		},

		 {
		  name:'Outils de développement',
		  competences:findCompetencesByNames(["Eclipse","Maven","Jenkins", "Artifactory", "Sonar", "Composer", "Astah"])
		},
		{
		  name:'WebService',
		  competences:findCompetencesByNames(["SOAP","REST"])
		}
	]
  }

];
