function myPageToPrint(sectionsContent){
	    
		var cartoucheSection = new Vue({
		  el:'#cartouche',	
		  props: {				   		  
				  cartouche: {
					  type: Object,
					  default: function () {				  		  
						return cartouche_data;
					  }
				  }
				  
		    },	  
		  template:		     
		     `<div>
                
                <p class="no-space"><span class="bolder">{{cartouche.nom}}, {{cartouche.situationpro}}</span>, {{cartouche.age}} ans, {{cartouche.situationperso}}, {{cartouche.nbEnfant}}.</p>                
                <p class="no-space">{{cartouche.adr}}-{{cartouche.cp}} {{cartouche.ville}}</p>                
                <p class="no-space">Mail : {{cartouche.mail}} / Téléphone : {{cartouche.tel}} / Site Personnel : {{cartouche.siteperso}}</p>                               
            

		</div>`
		  	
		});	    
		
		var languageSection = new Vue({
			el:'#language',
			props: {				   		  
				  language: {
					  type: Object,
					  default: function () {				  		  
						return findData(sectionsContent, "Langues");
					  }
				  }
		    },
			template:
			  `<div>
			  <h3>{{language.title}}</h3>
               
                    <ul>
                                    <li v-for="child in language.data">
                                        {{child.language}}:{{child.level}}
                                    </li>   
                    </ul>                
                </div>`
			
		});
		
		var competenceSection = new Vue({
			
			el: "#competence",
			props: {				   		  
				  competence: {
					  type: Object,
					  default: function () {				  		  
						return findData(sectionsContent, "Compétence");
					  }
				  },
				  competencesTab: {
					  type: String,
					  default: function () {
						  
						  var data = findData(sectionsContent, "Compétence").data;
						  var tabHeaders = `
						  <table id="competences">
							 <tr>
								 <th>Thème</th>
								 <th>Expert</th>
								 <th>Compétent</th>
								 <th>Avancé</th>
								 <th>Basique</th>
								 <th>Débutant</th>
							 </tr>						  
						  `
						  
						  var lineStr = "";
						  
						  data.forEach(function (competenceGroup){							  
							  
							  lineStr += "<tr>";
							  
							  lineStr += '<td>'+competenceGroup.name+'</td>';
							  
							  competence_level_table.forEach(function (level){ 
								  
							  lineStr += '<td>';
							  
							  var comps = sortCompetenceByLevel(competenceGroup.competences, level.name);
							  
							  var compCol = "";
							  
								  comps.forEach(function (competence){
									  compCol += competence+", ";
								  });
								  
							  compCol = compCol.substr(0, compCol.length-", ".length);
							  
							  lineStr += compCol;  
								  
							  lineStr += '</td>';
							  
							  });
							  
							  lineStr += "</tr>";
							  
						  });	
						  
						  										  
						  			  		  
						return tabHeaders+=lineStr+"</table>";
					  }
				  },
				  
		    },
			template:
			 `<div>
			 <h3>{{competence.title}}</h3>
			  <p><small>Classement réalisé en suivant <a href="https://en.wikipedia.org/wiki/Dreyfus_model_of_skill_acquisition">le modèle d'acquisition des connaissances de Dreyfus</a>.</small></p>
			 <div v-html="competencesTab"></div>			 
             </div>
              
			</div>`
			
		
		});
			
		   
		   var expSection =  new Vue({
			
			el: '#experience',			  
			props: {				   		  
				  experience: {
					  type: Object,
					  default: function () {		  		  
						return findData(sectionsContent, "Expérience");
					  }
				  }
		    },
			 template:
			  `<div><h3>{{experience.title}}</h3>   
			  <ul style="list-style-type:none;">
			  <li style="list-style-type:none;padding:1mm" v-for="child in experience.data">
				
					<span style="color:#BA4E4E;font-weight:bold">{{child.poste.name}}</span> pour {{child.poste.firmName}} -
					<small class="text-muted">
						
							({{child.poste.startYear}}-{{child.poste.endYear}}) |                                            
							{{child.poste.jobCityLoc}}, {{child.poste.jobCountryLoc}}
						
					</small>												
					<ul style="list-style-type:none;margin:0;padding:0">
						<li style="margin:0;padding:0" v-for="mission in child.missions">
							<span class="bolder"><small>Rôle</small> {{mission.label}}</span>
							  <ul>											  
							  <li v-for="action in mission.actions">
								{{action}}.
							   </li>
							  </ul>											  
						 </li>
					</ul>
					
				</li>
				<br />
			 </ul>
			 </div>`
		  
		   });
		  
		  /*template:
			  `<div><h3>{{experience.title}}</h3>   
			  <table>
			  <tr style="padding:0mm" v-for="child in experience.data">
			  <td style="padding:0mm">
				
					<span style="color:#BA4E4E;font-weight:bold">{{child.poste.name}}</span> pour {{child.poste.firmName}} -
					<small class="text-muted">
							({{child.poste.startYear}}-{{child.poste.endYear}}) |{{child.poste.jobCityLoc}}, {{child.poste.jobCountryLoc}}						
					</small>												
					<ul style="list-style-type:none;padding:0">
						<li style="padding:0" v-for="mission in child.missions">							
							<span class="bolder"><small>Rôle</small> {{mission.label}}</span>
							  <ul style="padding:0">								
								<li style="padding:0" v-for="action in mission.actions">
									{{action}}
								</li>
							   </ul>
						 </li>
					</ul>
					
				</td>
				</tr>
			 </table>
			 </div>`
		  
		   });	*/	   
		   
		   var divers = new Vue({
			
			el:'#divers',
			 props: {				   		  
				  divers: {
					  type: Object,
					  default: function () {				  		  
						return findData(sectionsContent, "Divers");
					  }
				  }
		    },
			template:
			 `<div><h3>{{divers.title}}</h3>
				<div>
				<ul>
				   <li v-for="child in divers.data">{{child}}</li>
				</ul>
				</div></div>`
			   
		   });
	  
	  var formationSection = new Vue({
			  
		  el:'#formation',
		  props: {				   		  
				  formation: {
					  type: Object,
					  default: function () {				  		  
						return findData(sectionsContent, "Formation");
					  }
				  }
		  },
		  template:
		  `<div><h3>{{formation.title}}</h3>
                <ul style="list-style-type:none">                    
                        <li style="list-style-type:none;padding:1mm" v-for="child in formation.data">
							<span class="bolder">{{child.title}}</span>
							- <small>{{child.cityLocation}}, {{child.countryLocation}} | ({{child.startYear}} - {{child.endYear}})</small>   
							
							<p class="no-space">
								                                           
										{{child.certificateTitle}}, {{child.optionTitle}}                                            
								                                        
							</p>  
                               
                        </li>
               </ul>
			   <p></p>
			 </div>`
	  });
	  
	  return false;	 	

   }	  
