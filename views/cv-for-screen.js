function newSelectionProfil(){
	
	var cartoucheSection = new Vue({
		  el:'#cartouche',	
		  props: {				   		  
				  cartouche: {
					  type: Object,
					  default: function () {						  
						 return cartouche_data;
					  }
				  },
				  profils_list: {
					  type: Array,
					  default: function () {						
						
						var profilID = param("profil");
						
						var groupId = "moe";

						var selected = "selected";
						
						var profils=[];
						
						profil_name_table.forEach(function(profil){
							
							if(profilID == profil.id){
								selected = "selected";
								groupId = profil.type;
							}
							else{
								selected = "";
							}

							profils.push({
								type: profil.type,
								choosen :selected,
								label: profil.name
							});
						});								
							
							newCV(groupId);
					  
						return profils;
						
						
					  }					  
					  
				  }				  
				 
		    },	  
		  template:
		     `<div>
		      <div class="col-sm-4">
                <img src="assets/img/triaubaral.png" title="Avatar" alt="My Avatar">
                <h4>{{cartouche.nom}}</h4>                
                <p class="no-space">{{cartouche.adr}}</p>
				<p>{{cartouche.cp}} {{cartouche.ville}}</p>                
                <p><a href="mailto:this.text"><i class="fa fa-envelope-o fa-fw"></i>{{cartouche.mail}}</a></p>
                <p>Téléphone : {{cartouche.tel}}</p>
                
                <p>
                <a href="javascript:printCV();" class="btn btn-primary" title="Curriculum Vitae">
				  <span class="glyphicon glyphicon-print"></span> CV 
				</a>
				<a class="btn btn-primary" href="http://triaubaral.gitlab.io/whoami/pdf/memoire.pdf" title="Mémoire d\'ingénieur">
					<span class="fa fa-file-pdf-o"></span>&nbsp;Mémoire</a>
                </p>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-4 visible-md-block visible-lg-block">              
             <h2><label on="profils">Profil</label>:
			 
			 <select id="profils" data-live-search="true" onchange="newCV(this.value)" data-width="75%" class="selectpicker">
				<option v-for="profil in profils_list" :selected="profil.choosen" :value="profil.type">{{profil.label}}</option>
			 </select>
             </h2>              
            </div>
            <div class="col-sm-4 visible-md-block visible-lg-block visible-sm-block">
                <p><i class="fa fa-github fa-fw"></i><a href="//github.com/triaubaral" target="_blank">  @triaubaral </a></p>
                <p><i class="fa fa-linkedin-square fa-fw"></i> <a href="//linkedin.com/in/atricoire" target="_blank"> linkedin.com/in/atricoire </a></p>
                <p><i class="fa fa-twitter fa-fw"></i> <a href="//twitter.com/triaubaral" target="_blank">  @triaubaral </a></p>
             </div>
            </div>`
		  	
		});
	
}

function myPage(sectionsContent){
	   		
			
		var expSection =  new Vue({
			
			el: '#experience',			  
			props: {				   		  
				  experience: {
					  type: Object,
					  default: function () {		  		  
						return findData(sectionsContent, "Expérience");
					  }
				  }
		    },
		  template:
		  `<div><h3 class="title with-icon"><span class="glyphicon glyphicon-paperclip cat-title"></span>{{experience.title}}</h3>   
		  <ul class="timeline">
			<li class="timeline-inverted" v-for="child in experience.data">
                            
                           <div class="timeline-badge info">{{child.poste.startYear}}</div>
                            
                           <div class="timeline-panel grid-block">
                                <div class="timeline-heading">
                                    
                                        <h4 class="timeline-title"><span style="color:#BA4E4E;font-weight:bolder">{{child.poste.name}}</span> pour {{child.poste.firmName}}</h4>
                                    
                                    
                                    <p>
                                        <small class="text-muted">
                                            
                                                <i class="fa fa-calendar"></i> ({{child.poste.startYear}}-{{child.poste.endYear}}) |                                            
                                                <i class="fa fa-map-marker"></i>  {{child.poste.jobCityLoc}}, {{child.poste.jobCountryLoc}}
                                            
                                        </small>
                                    </p>
                                </div>
                                <div class="timeline-body">                                        
                                        <ul>
                                        <li v-for="mission in child.missions">
											<h4><small>Rôle</small> {{mission.label}}</h4>
											  <ul>											  
											  <li v-for="action in mission.actions">
												{{action}}
											   </li>
											  </ul>											  
                                         </li>
                                        </ul>                                    
                                </div>
                                
                            </div>
							
                        </li>
                         </ul></div>`		
		  
		   });	
		   
		  var formationSection = new Vue({
			  
		  el:'#formation',
		  props: {				   		  
				  formation: {
					  type: Object,
					  default: function () {				  		  
						return findData(sectionsContent, "Formation");
					  }
				  }
		  },
		  template:
		  `<div><h3 class="title with-icon"><span class="glyphicon glyphicon-education cat-title"></span>{{formation.title}}</h3>
                <ul class="timeline">                    
                        <li class="timeline-inverted" v-for="child in formation.data">                            
                            <div class="timeline-badge info">{{child.startYear}}</div>                            
                            <div class="timeline-panel grid-block">
                                <div class="timeline-heading">                                    
                                       <h4 class="timeline-title">{{child.title}}</h4>                                    
                                    <p>
                                        <small class="text-muted">                                            
                                                {{child.certificateTitle}}, {{child.optionTitle}}                                            
                                        </small>
                                    </p>
                                    <p>
                                        <small class="text-muted">                                            
                                                <i class="fa fa-calendar"></i> ({{child.startYear}} - {{child.endYear}}) | 
                                                <i class="fa fa-map-marker"></i>  {{child.cityLocation}}, {{child.countryLocation}}
                                        </small>
                                    </p>
                                </div>
                                <div class="timeline-body">
                                        <p>{{child.description}}</p>
                                </div>
                            </div>
                        </li>
               </ul></div>`
		  
		  
	  }); 
		   
		   
		  var culture = new Vue({
			
			el:'#culture',
			 props: {				   		  
				  culture: {
					  type: Object,
					  default: function () {				  		  
						return findData(sectionsContent, "Culture");
					  }
				  }
		    },
			template:
			 `<div>
				<h3 class="title with-icon"><span class="glyphicon glyphicon-signal cat-title"></span>{{culture.title}}</h3>
				<div class="grid-block">
				   <ul>
						<li v-for="child in culture.data">
							<h4>{{child.theme}}</h4>
						    <ul>
							<li v-for="item in child.items">
								<a :href="item.url">{{item.text}}</a>
							</li>
							</ul>
				        </li>
				  </ul>
				</div>
			 </div>`
			   
		   });
		   
		  
		   
		   var divers = new Vue({
			
			el:'#divers',
			 props: {				   		  
				  divers: {
					  type: Object,
					  default: function () {				  		  
						return findData(sectionsContent, "Divers");
					  }
				  }
		    },
			template:
			 `<div><h3 class="title with-icon"><span class="glyphicon glyphicon-heart cat-title"></span>{{divers.title}}</h3>
				<div class="grid-block">
					<ul>
					   <li v-for="child in divers.data">{{child}}</li>
					</ul>
				</div>
			 </div>`
			   
		   });


		var competenceSection = new Vue({
			
			el: "#competence",
			props: {
	
	              rate: {
					  type: Map,
					  default: function () {	
			  		  
						var levelRate = new Map();						
						competence_level_table.forEach(function(level){
							levelRate.set(level.name, "width:"+level.rate+"%");
						});
			   
						return levelRate;
						
					  }
				  },			
				  competence: {
					  type: Object,
					  default: function () {				  		  
						return findData(sectionsContent, "Compétence");
					  }
				  },
				  legende: {
					  type: Map,
					  default: function () {
					  
					     var legend = new Map();
						 findData(sectionsContent, "Légende").data.forEach(function(item){
						     
							 var description = "<ul>";
							 
							 item.description.forEach(function(action){
							 
							     description +="<li>"+action+"</li>";
							 
							 });
							 
							 description += "</ul>";
							 
							 legend.set(item.name,description);
						 
						 });
							
						return legend;
					  }
				  },
				  legendObj: {
					  type: Object,
					  default: function () {			     
							
						return findData(sectionsContent, "Légende");
					  }
				  }
		    },
			template:
			 `<div>
			 <h3 class="title with-icon"><span class="fa fa-code cat-title"></span>{{competence.title}} <a onclick="dispLegend()" title="Afficher la légende de notation" class="glyphicon glyphicon-info-sign" href="#"></a></h3>
			 <div>			 
			 <table id="legendeTab" style="display:none">
				<tr style="border: 1px solid black;" v-for="leg in legendObj.data">
					<td style="border: 1px solid black;padding:5px">{{leg.name}}</td>
					<td>
						<ul>
							<li v-for="action in leg.description">{{action}}</li>						
						</ul>
					
					</td>
			    </tr>
			 </table>
			 </div>
			 <div class="grid-block" v-for="child in competence.data">			
			 <h4>{{child.name}}</h4>
                    <ul class="list-unstyled list-skills">                        
                           <li v-for="detail in child.competences">
                                    <span class="caption-skill">{{detail.name}}</span>   
                                    <div class="progress">
                                        <div class="progress-bar" :style="rate.get(detail.level)">
                                           <span name="skill-level" data-html="true" data-placement="auto" data-toggle="tooltip" :title="legende.get(detail.level)">{{detail.level}}</span>
                                        </div>
                                    </div>                                
                            </li>                        
                    </ul>
                </div>
                <div class="deviter"></div>
			</div>`
			
		
		});	


		var localisationSection = new Vue({
			el:'#localisation',
			props: {				   		  
				  localisation: {
					  type: Object,
					  default: function () {				  		  
						return findData(sectionsContent, "Géolocalisation");
					  }
				  }
		    },
			template:
			`<div>
			<h3 class="title with-icon"><span class="fa fa-location-arrow cat-title"></span>{{localisation.title}}</h3>
            <div class="grid-block">
                <iframe :src="localisation.place" width="100%" height="100%" frameborder="0" style="border:0"></iframe>
            </div>
            </div>`
			
		});
			  
	    var languageSection = new Vue({
			el:'#language',
			props: {				   		  
				  language: {
					  type: Object,
					  default: function () {				  		  
						return findData(sectionsContent, "Langues");
					  }
				  }
		    },
			template:
			  `<div>
			  <h3 class="title with-icon"><span class="fa fa-globe cat-title"></span>{{language.title}}</h3>
               
                    <ul class="list-unstyled list-strip">
                                    <li v-for="child in language.data">
                                        <i class="fa fa-check"></i> <span class="badge badge-green badge-right-float">{{child.level}}</span>{{child.language}}
                                    </li>   
                    </ul>
                <div class="deviter"></div>
                </div>`
			
		});		

   }	  
